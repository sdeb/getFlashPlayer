
# changelog getFlashPlayer

tags utilisés: added  changed  cosmetic  deprecated  fixed  rewriting  removed  syncro  security
date au format: YYYY-MM-DD

## [ Unreleased ]

## [ 4.17.1 ] - 2019.07.08

* rewriting f__wget_test
* rewriting: récup versions on ligne: fflash_get_version
* syncro: fscript_install, fscript_remove, fscript_update, f__user

## [ 4.16.0 ] - 2019.05.04

* fixed: bash v5 utilisable (v4+)

## [ 4.15.0 ] - 2018.07.03

* synchro: f__requis, f__sort_uniq

## [ 4.14.1 ] - 2018.06.13

* rewriting: f__archive_test, fflash_get_version
* rewriting: suppression sed peu utiles
* cosmetic: exit sur --help

## [ 4.14.0 ] - 2018.06.12

* syncro: composants, fscript_update avec ses parametres d'appel
* fix: fflash_install, effacement correct archive téléchargée
* fix: droits corrects sur téléchargement si root
* fix: création répertoire temporaire téléchargement 

## [ 4.11.0 ] - 2018.06.11

* added: téléchargement seul
* fix: version archive lors téléchargement manuel

## [ 4.10.0 ] - 2018.06.11

* added: f__trim
* added: option --dev
* syncro; composants
* rewriting: traitement options d'appel
* rewriting: shellcheck
* cosmetic: affiche help & affichage
* fix: danger potentiel rm -fr
* fix: url

## [ 4.9.0 ] - 2018.03.04

* syncro: f__color, f__info, f__sudo, f__user, f__wget_test
* syncro: fscript_install, fscript_remove, fscript_update
* rewriting: prg_init, f_help
* rewriting: général ubuntu 16.04

## [ 4.8.2 ] - 2018.02.11

* syncro: f__color

## [ 4.8.1 ] - 2018.01.29

révision: +requis awk>gawk

## [ 4.8.0 ] - 2018.01.26

* rewriting: mineur, fscript_cronAnacron fscript_install fscript_remove fscript_update
* rewriting: f__requis
* fixed: f__sudo, extraction nb tentatives

## [ 4.6.0 ] - 2018.01.25

* rewriting: fflash_install, changement nom archive

## [ 4.5.0 ] - 2018.01.24

* added: cumul options (opérations) possibles (sauf opés scripts)
* added: option --sauve pour conserver le téléchargement à l'installation
* rewriting: invocation f__sudo dans traitement options, plus confortable si su & _all_
* rewriting; f__wget_test
* rewriting: f_sudo abandonné dans fscript_install et fscript_remove, au profit appel au traitement général des options
* rewriting: f_help, f_affichage
* rewriting: fflash_avertissement
* rewriting: général wget_log: fscript_get_version, fscript_update
* fixed: détection si installé lors upgrade, stand-by possible
* fixed: erreur wget-log quand passage en root

## [ 4.4.0 ] - 2018.01.14

* rewriting: f_sudo, format nombre de tentatives et options appel possibles > 1

## [ 4.3.0 ] - 2018.01.13

*  révision: auto-installation, potentiel bug selon conditions appel

## [ 4.2.0 ] - 2018.01.12

* fixed: correction commentaire fscript_get_version

## [ 4.1.0 ] - 2017.12.29

* fixed: fscript_cronAnacron, erreurs mineures à la suppression du script

## [ 4.0.0 ] - 2017.12.27

* rewriting: complet
* syncro: fonctions

## [ 3.21.0 ] - 2017.12.18

* fixed: install manuelle, bug potentiel

## [ 3.20.0 ] - 2017.12.13

* rewriting: f__wget_test

## [ 3.19.0 ] - 2017.12.6

* rewriting: fscript_update, controle chargement début et fin
* rewriting: changement séquence start pour éviter erreur cron

## [ 3.18.1 ] - 2017.12.5

* rewriting: démarrage
* rewriting: synchro fonctions communes
* rewriting: fflash_install, fscript_cronAnacron_special, fflash_remove, renommage user_
* rewriting: suppression avertissement  ou remove flashplugin-nonfree
* rewriting: fflash_get_version, gestion des retours en cas de n/a
* rewriting: général, mise en forme
* fixed: fflash_remove
* fixed: f__wget_test

## [ 3.17.0 ] - 2017.10.16

* rewriting: f__error f__info f__requis f__wget_test 

## [ 3.16.0 ] - 2017.10.11

* fixed: f__sudo : fonctionnement avec sudo

## [ 3.15.0 ] - 2017.10.08

* rewriting: f__wget_test(): nouvelle option test, nouveau nommage fichier temp
* rewriting: f__user, premier essai root only, fonctionnement en root only en console
* added: test bash4 au démarrage
* rewriting: f__color: utilisation terminfo pour retour au std (et non noir), donc modifs:
    * f__color f__error f__info f__wget_test
    * fscript_get_version fscript_install fscript_remove fscript_update
* syncro: f__sudo dans install & remove script

## [ 3.14.0 ] - 2017.09.23

* syncro: f__requis, f__info, f__error	unset/for
* rewriting: 	unset/for

## [ 3.13.0 ] - 2017.09.07

* fixed: f_help
* rewriting: f__wget_test, fscript_get_version, f__log

## [ 3.12.0 ] - 2017.09.06

* syncro: fscript_cronAnacron, fscript_update, fscript_install, fscript_remove
* cosmetic: dirTemp, user_

## [ 3.11.0 ] - 2017.09.04

* added: IFS

## [ 3.10.0 ] - 2017.09.03

* rewriting: appel fscript_remove, fscript_install & fscript_update

## [ 3.9.1 ] - 2017.09.01

* rewriting: f__wget_test

## [ 3.9.0 ] - 2017.08.30

* rewriting: f__requis, f__user, f__wget_test, fscript_cronAnacron
* rewriting: déclaration local
* rewriting: f__archive_test

## [ 3.8.0 ] - 2017.08.30

* rewriting: conditions d'utilisations, fscript_install, fscript_remove(), fscript_update
* rewriting: appel, fscript_install, fscript_remove(), fscript_update
* cosmetic: f_help 

## [ 3.7.0 ] - 2017.08.28

* fixed: localisation fileDev
* rewriting: f__wget_test
* fixed: pas d'affichage progression dans logs mail, fflash_install

## [ 3.6.0 ] - 2017.08.27

* fixed: install
* cosmetic: fscript_cronAnacron, fscript_install, changement lognameDev ->fileDev
* rewriting: présentation fscript_get_version, fscript_install, fscript_remove

## [ 3.5.0 ] - 2017.08.26

* cosmetic: fscript_dl en fscript_update
* rewriting: fscript_install, fscript_update, fscript_get_version
* rewriting: f__wget_test
* rewriting: f__error, f__info
* fixed: fscript_install pour éventuel fscript_install_special
* fixed: bug upgrade

## [ 3.2.2 ] - 2017.08.24

* added: avertissement pas de maj plugin

## [ 3.2.1 ] - 2017.08.23

* rewriting: changement délais anacron, fscript_cronAnacron

## [ 3.2.0 ] - 2017.08.22

* fixed: fix $TERM
* syncro: fscript_dl

## [ 3.1.1 ] - 2017.08.22

* syncro: mineure fscript_dl

## [ 3.1.0 ] - 2017.08.21

* révison: f__user
* log: pas de maj script
* syncro: fscript_dl, fscript_install, fscript_remove, f__info

## [ 3.0.9 ] - 2017.08.20

* rewriting: maj plugin sous cron
* fixed: fscript_cronAnacron appel fscript_cronAnacron_special
* fixed: $TERM

* syncro: fscript_cronAnacron & plus de redémarrage service cron inutile & fonction spécifique pour certains scripts service cron restart &>/dev/null || /etc/init.d/cron restart &>/dev/null || f__info "redémarrer cron ou le PC"
* rewriting: fscript_dl plus de sortie progression download
* rewriting: fscript_get_version inclut version en cours
* rewriting: fscript_install mise en page
* rewriting: fflash_install_manuel fflash_install

## [ 3.0.6 ] - 2017.08.18

* syncro: fscript_cronAnacron lors upgrade et spécial pour dev 
* syncro: fscript_get_version, fscript_dl, fscript_install
* rewriting: vérification requis pour fonctionnement script

## [ 3.0.5 ] - 2017.08.17

* rewriting: test inscription crontab pour recherche bug siduction
* rewriting: test crontab et modif anacrontab lors upgrade

## [ 3.0.2 ] - 2017.08.16

* rewriting: homogénéisation des fonctions de script , localisation anacrontab 
  **réinstallation manuelle nécessaire (root)**
* rewriting: chmod
* fixed: maj non fonctionnelle avec fonction f__user récente.. :(
* rewriting: f__user
* rewriting: f__requis

## [ 2.9.0 ] - 2017.07.31

* fixed: détection user sous gnome

## [ 2.8.2 ] - 2017.07.31

* added: option version script

## [ 2.8.1 ] - 2017.07.30

* cosmetic:
* fixed: bug mineur: pas d'inscription superflue dans crontab si plusieurs installations scripts successives
* cosmetic: anacrontab

## [ 2.7.0 ] - 2017.07.28

* changed: fichier log spécifique mais nécessite réinstallation en root (pas de mise à jour)
* changed: logs d'évènements majeurs
* fixed: suppression ancienne localisation /usr/local/bin/
* added: avertissement maj manuelle à faire pour nouveaux logs 

## [ 2.6.7 ] - 2017.07.27

* removedd: installation en user si préexistant 
* changed: détection paquet installé
* rewriting: grep quiet

## [ 2.6.6 ] - 2017.07.26

* cosmetic:

## [ 2.6.5 ] - 2017.07.24

* rewriting: séparation mise à jour script et plugin, le script peut être en stand-by, se mettre à jour, sans plugin installé
* rewriting: lien script dans /usr/bin/ (meilleure compat?)
* cosmetic:

## [ 2.6.2 ] - 2017.07.18

* changed: ajouts chown sécu
* rewriting: ménage tests supplémentaires
* cosmetic:
* fixed: sécu
* fixed: meilleur ménage lors mise à jour script

## [ 2.5.0 ] - 2017.07.18

* fixed: chmod sur dl script

## [ 2.4.3 ] - 2017.07.17

* changed: test dépendances, test cnx
* rewriting: extraction version flash
* rewriting: procédure maj script
* rewriting: wgets
* fixed: ménage tentative détection url v2
* fixed: lien fr pour détection version en ligne
* fixed: typo

## [ 2.3.1 ] - 2017.07.17

* rewriting: affichage upgrade
* rewriting: affichage/log erreurs/infos

## [ 2.3.0 ] - 2017.07.16

* fixed: erreur chargement retour à url v1 fonctionnant
* fixed: remove plugin: enlève tache anacron & mauvaise version affichée
* changed: multi linux: détection dpkg avant purge flashplugin-nonfree éventuel
* changed:  gestion syslog

## [ 2.2.1 ] - 2017.07.14

* fixed: mauvaise détection paquet flashplugin-nonfree installé
* changed: installation script optimisée lors mise à jour
* changed: root plus nécessaire lors maj manuelle du script
* changed: déclaration PATH & IFS (sécu)
* fixed: mineurs
* fixed: détection changement url
* fixed: mineur: user-agent
* fixed: erreur chargement -> wget? idem avec curl

## [ 2.1.0 ] - 2017.07.13

* added: installation d'une archive téléchargée manuellement
* changed: test validité archive téléchargée


## [ 2.0.2 ] - 2017.07.13

* added:
   * mise à jour automatique du script
   * chargements moins verbeux
   * wget: message debug 
   * test et maj éventuelle script lors du test disponibilité de flash 
   * added de limitation debian
   * abandon version v-test (problème de liens de chargement)
* fixed: mineurs
* cosmetic: fonctions
* fixed: test sur taille download 
* fixed: test wget

## [ 1.1.0 ] - 2017.07.12

* fixed: user non reconnu sous gnome
* added: contournement éventuel pour imposer un utilisateur où installer (ou si échec sous debian7?)

## [ 1.0.2 ] - 2017.07.02

* fixed: renommage appel installation version de test: v-test (au lieu) de test

## [ 1.0.1 ] - 2017.06.22

* fixed: suppression lignes de test commentées
* fixed: typos

## [ 1.0.0 ] - 2017.06.21

  * 1ère publication
