# getFlashPlayer


![version: 4.17.1](https://img.shields.io/badge/version-4.17.1-blue.svg?longCache=true&style=for-the-badge)
![bash langage](https://img.shields.io/badge/bash-4-brightgreen.svg?longCache=true&style=for-the-badge)
![license LPRAB / WTFPL](https://img.shields.io/badge/license-LPRAB%20%2F%20WTFPL-blue.svg?longCache=true&style=for-the-badge)


> script bash qui télécharge et installe la version officielle du plugin FlashPlayer 
  pour Firefox (NPAPI)  
> le script installé sera disponible pour les différents canaux en place, pour un utilisateur donné
> le script installe le plugin pour l'utilisateur en cours, comme plugin utilisateur.  
> le script surveillera et mettra à jour le plugin lors d'une nouvelle version de FlashPlayer  
> le script se mettra éventuellement à jour, sans influence sur le plugin installé   
> le script peut désinstaller le plugin  
> seul l'installation et la désinstallation du **script** requièrent les privilèges **root**.   
> les opération concernant le plugin se font en tant qu'utilisateur
> script testé sur debian / ubuntu, mais devrait être compatible avec d'autres distributions



## installation rapide du script

* privilèges **root** requis

```shell
wget -nv -O getFlashPlayer  https://framaclic.org/h/getflashplayer
chmod +x getFlashPlayer && ./getFlashPlayer
```
```text
              _   _____ _           _     ____  _                       
    __ _  ___| |_|  ___| | __ _ ___| |__ |  _ \| | __ _ _   _  ___ _ __ 
   / _' |/ _ \ __| |_  | |/ _' / __| '_ \| |_) | |/ _' | | | |/ _ \ '__|
  | (_| |  __/ |_|  _| | | (_| \__ \ | | |  __/| | (_| | |_| |  __/ |   
   \__, |\___|\__|_|   |_|\__,_|___/_| |_|_|   |_|\__,_|\__, |\___|_|   
   |___/    version 4.11.0 - 11/06/2018                 |___/  pour Firefox
                

  getFlashPlayer 4.11.0 installé dans le système.
  maintenant, appel du script par: getFlashPlayer (sans ./)

```

* le script est maintenant dans le système et tout utilisateur peut s'en servir
* **FlashPlayer n'est pas encore installé**
* le script est inscrit dans la crontab utilisateur, une mise à jour du plugin et du script sera faite
  périodiquement si disponible et si le plugin est installé


## installation plugin

```shell
getFlashPlayer install
```
```text
              _   _____ _           _     ____  _                       
    __ _  ___| |_|  ___| | __ _ ___| |__ |  _ \| | __ _ _   _  ___ _ __ 
   / _' |/ _ \ __| |_  | |/ _' / __| '_ \| |_) | |/ _' | | | |/ _ \ '__|
  | (_| |  __/ |_|  _| | | (_| \__ \ | | |  __/| | (_| | |_| |  __/ |   
   \__, |\___|\__|_|   |_|\__,_|___/_| |_|_|   |_|\__,_|\__, |\___|_|   
   |___/    version 4.11.0 - 11/06/2018                 |___/  pour Firefox
                

  FlashPlayer n'est pas libre, c'est un programme propriétaire dont on ne peut pas connaître 
  le code source, que l'on ne peut utiliser sans conditions, ni même distribuer librement.
  Il souffre de nombreuses vulnérabilités chroniques, depuis des années...
  Il sera plus ou moins bloqué à terme pas les navigateurs majeurs,
  c'est à dire Firefox et Chrom(ium). Préparez vous à cela...
  voir: https://developer.mozilla.org/fr/docs/Plugins/Roadmap

  installation FlashPlayer, version 30.0.0.113

    - téléchargement...

/tmp/getFlashPlayer/flashplayer_30.0.0.113_npapi 100%[========================>]   8,63M  7,53MB/s    ds 1,1s    
  
    - décompression...

  FlashPlayer 30.0.0.113 installé
    Pour tester: http://get.adobe.com/flashplayer/about/

```

* FlashPlayer est installé, en étant directement chargé du site Adobe
* chaque utilisateur du système pourra/devra lancer le script pour installer le plugin
* **seule la bibliothèque** est installée comme plugin Firefox. FlashPlayer pourra être configuré
  au clic droit dans le navigateur. le **système n'est pas encombré inutilement** 
* pour vérifier le bon fonctionnement, vous pouvez aller [sur le site](http://get.adobe.com/flashplayer/about/)
* _théoriquement_, inutile de redémarrer Firefox
* en cas de soucis, vérifier dans `Outils/Modules complémentaires/plugins`: _shockwave flash_ doit être **activé**



## help

```shell
getFlashPlayer -h
```
```text
              _   _____ _           _     ____  _                       
    __ _  ___| |_|  ___| | __ _ ___| |__ |  _ \| | __ _ _   _  ___ _ __ 
   / _' |/ _ \ __| |_  | |/ _' / __| '_ \| |_) | |/ _' | | | |/ _ \ '__|
  | (_| |  __/ |_|  _| | | (_| \__ \ | | |  __/| | (_| | |_| |  __/ |   
   \__, |\___|\__|_|   |_|\__,_|___/_| |_|_|   |_|\__,_|\__, |\___|_|   
   |___/    version 4.11.0 - 11/06/2018                 |___/  pour Firefox
                
  -----------------------------------------------------------------------
  getFlashPlayer install :  installation de Flashplayer
  getFlashPlayer remove  :  désinstallation de Flashplayer
  getFlashPlayer tc      :  téléchargement dans le répertoire courant (sans installation)
  getFlashPlayer upgrade :  mise à jour plugin si disponible
  getFlashPlayer version :  versions de Flashplayer et du script, en ligne et en place

  getFlashPlayer manuel archive.tar.gz : installation d'une archive téléchargée manuellement

    --dev   : une version de dev du script (si existante) est recherchée
    --sauve : le téléchargement est sauvegardé dans le répertoire courant en plus de l'installation
  -----------------------------------------------------------------------
  ./getFlashPlayer (ou ./getFlashPlayer -i) : installation du script dans le système (root)
  getFlashPlayer -h, --help    : affichage aide
  getFlashPlayer -r, --remove  : désinstallation du script (root)
  getFlashPlayer -u, --upgrade : mise à jour script & Flashplayer
  getFlashPlayer -v, --version : version du script
  -----------------------------------------------------------------------
    plus d'infos:  https://framaclic.org/h/doc-getflashplayer
  -----------------------------------------------------------------------
  FlashPlayer n'est pas libre, c'est un programme propriétaire dont on ne peut pas connaître 
  le code source, que l'on ne peut utiliser sans conditions, ni même distribuer librement.
  Il souffre de nombreuses vulnérabilités chroniques, depuis des années...
  Il sera plus ou moins bloqué à terme pas les navigateurs majeurs,
  c'est à dire Firefox et Chrom(ium). Préparez vous à cela...
  voir: https://developer.mozilla.org/fr/docs/Plugins/Roadmap

```


## version

```shell
getFlashPlayer version
```
```text
              _   _____ _           _     ____  _                       
    __ _  ___| |_|  ___| | __ _ ___| |__ |  _ \| | __ _ _   _  ___ _ __ 
   / _' |/ _ \ __| |_  | |/ _' / __| '_ \| |_) | |/ _' | | | |/ _ \ '__|
  | (_| |  __/ |_|  _| | | (_| \__ \ | | |  __/| | (_| | |_| |  __/ |   
   \__, |\___|\__|_|   |_|\__,_|___/_| |_|_|   |_|\__,_|\__, |\___|_|   
   |___/    version 4.11.0 - 11/06/2018                 |___/  pour Firefox
                

  script en place: 4.11.0
  script en ligne: 4.11.0

  FlashPlayer en place: 30.0.0.113
  FlashPlayer en ligne: 30.0.0.113

```


## désinstallation plugin

```shell
getFlashPlayer remove
```

* le plugin est effacé du profil _.mozilla_ de l'utilisateur
* cela **ne supprime pas le script** _getFlashPlayer_ du système


## mise à jour plugin

```shell
getFlashPlayer upgrade
```

* mise à jour du **plugin** si une nouvelle version est disponible en ligne
* cette tâche est exécutée périodiquement par cron/anachron et n'a pas vraiment vocation à être 
  lancée manuellement
* _anacron_ est utilisé, c'est à dire que la mise à jour sera testée, dès le redémarrage du Pc



## installation manuelle

* [charger manuellement](https://get.adobe.com/fr/flashplayer/) Flashplayer en choisissant une 
  archive linux tar.gz - **NPAPI**
* lancer l'installation habituelle, avec:   

```shell
getFlashPlayer manuel flash_player_npapi_linux.x86_64.tar.gz
```


## mise à jour du script (et du plugin)

```shell
getFlashPlayer -u
```

* test toutes les **semaines**
* mise à jour du **script** si une nouvelle version est disponible en ligne
* le script se mettra à jour même si le plugin n'est pas installé 
* cette tâche est exécutée périodiquement par cron/anachron et n'a pas vraiment vocation à être lancée manuellement
* _anacron_ est utilisé, c'est à dire que la mise à jour sera testée, dès le redémarrage du Pc


## logs

```shell
cat /var/log/sdeb_getFlashPlayer.log
```

tous les évènements importants sont consignés dans le fichier _/var/log/sdeb_getFlashPlayer.log_   


## supprimer le script

```shell
getFlashPlayer -r
```

* privilèges **root** requis
* effacement du script dans le système (_/opt/bin_)
* effacement de l'inscription dans crontab/anacron utilisateur
* cela ne **supprime pas** un éventuel **plugin** installé


## sources

sur [framagit](https://framagit.org/sdeb/getFlashPlayer/blob/master/getFlashPlayer)


## changelog

sur [framagit](https://framagit.org/sdeb/getFlashPlayer/blob/master/CHANGELOG.md)


## contact

pour tout problème ou suggestion concernant ce script, n'hésitez pas à ouvrir une issue 
[Framagit](https://framagit.org/sdeb/getFlashPlayer/issues)

IRC freenode: ##sdeb


## license

[LPRAB/WTFPL](https://framagit.org/sdeb/getFlashPlayer/blob/master/LICENSE.md)


![compteur](https://framaclic.org/h/getflashplayer-gif)
